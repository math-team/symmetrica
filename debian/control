Source: symmetrica
Section: math
Priority: optional
Maintainer: Debian Math Team <team+math@tracker.debian.org>
Uploaders: Jerome Benoit <calculus@rezozer.net>
Rules-Requires-Root: no
Build-Depends: dpkg-dev (>= 1.22.5),
 debhelper-compat (= 13),
 gnulib,
 autoconf-archive,
 libtool,
 pkgconf
Standards-Version: 4.7.0
Homepage: https://gitlab.com/sagemath/symmetrica
Vcs-Git: https://salsa.debian.org/math-team/symmetrica.git
Vcs-Browser: https://salsa.debian.org/math-team/symmetrica

Package: libsymmetrica3
Provides: ${t64:Provides}
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: symmetrica-doc
Multi-Arch: same
Description: Symmetrica Combinatoric C Library -- library package
 Symmetrica is a library for combinatorics. It has support for the
 representation theory of the symmetric group and related groups,
 combinatorics of tableaux, symmetric functions and polynomials,
 Schubert polynomials, and the representation theory of Hecke algebras
 of type A_n.
 .
 This package provides the shared library required to run programs
 compiled against the Symmetrica Combinatoric C Library. To compile
 your own program you also need to install the libsymmetrica-dev
 package.

Package: libsymmetrica-dev
Section: libdevel
Architecture: any
Depends: libsymmetrica3 (= ${binary:Version}), ${misc:Depends}
Suggests: symmetrica-doc, pkgconf
Multi-Arch: same
Description: Symmetrica Combinatoric C Library -- development package
 Symmetrica is a library for combinatorics. It has support for the
 representation theory of the symmetric group and related groups,
 combinatorics of tableaux, symmetric functions and polynomials,
 Schubert polynomials, and the representation theory of Hecke algebras
 of type A_n.
 .
 This package contains the header files, static libraries and symbolic
 links that developers using the Symmetrica Combinatoric C Library will
 need.

Package: symmetrica-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Suggests: libsymmetrica-dev (= ${binary:Version})
Multi-Arch: foreign
Description: Symmetrica Combinatoric C Library -- documentation
 Symmetrica is a library for combinatorics. It has support for the
 representation theory of the symmetric group and related groups,
 combinatorics of tableaux, symmetric functions and polynomials,
 Schubert polynomials, and the representation theory of Hecke algebras
 of type A_n.
 .
 This package provides documentation for the Symmetrica Combinatoric C
 Library; it also contains examples.
